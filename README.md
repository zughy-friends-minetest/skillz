# Skills

This Minetest library allows you to create skills, such as a super-high jump, a damaging aura, or passive stat boosts. These skills can be used to enhance gameplay or add new features to your mod.

## Config

You can find the `SETTINGS.lua` configuration file in the `<YOUR_WORLD>/skills/` folder.

## Dev Documentation

Check the [dev documentation](https://gitlab.com/zughy-friends-minetest/skills/-/blob/main/DOCS.md?ref_type=heads) if you're a developer!