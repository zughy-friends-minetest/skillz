--
-- skill instance management: start/stop/enable/disable, lifecycle management: passive casting, basic checks...
--

skills.blocking_skills = {}  -- {"pl_name" = "mod:active_skill"}, to disable skills non-permanently

local S = core.get_translator("skills")
local NS = function (string) return string end

local get_player_by_name = core.get_player_by_name
local string_metatable = getmetatable("")





--
--
-- CALLBACKS
--
--

core.register_on_joinplayer(function(player)
	-- after to make sure other mods initialized
	core.after(0, function()
		skills.cast_passive_skills(player:get_player_name())
	end)
end)



core.register_on_leaveplayer(function(player, timed_out)
	local pl_name = player:get_player_name()
	local pl_skills = pl_name:get_unlocked_skills()

	for skill_name, def in pairs(pl_skills) do
		def:stop()
	end
end)



core.register_on_respawnplayer(function(player)
	skills.cast_passive_skills(player:get_player_name())
end)





--
--
-- PUBLIC API
--
--

function skills.cast_skill(pl_name, skill_name, ...)
	local skill = pl_name:get_skill(skill_name)

	if skill then
		return skill:cast(...)
	else
		return false
	end

end
string_metatable.__index["cast_skill"] = skills.cast_skill



function skills.start_skill(pl_name, skill_name, ...)
  local skill = pl_name:get_skill(skill_name)

	if skill then
		return skill:start(...)
	else
		return false
	end

end
string_metatable.__index["start_skill"] = skills.start_skill



function skills.stop_skill(pl_name, skill_name)
  local skill = pl_name:get_skill(skill_name)

	if skill then
		return skill:stop()
	else
		return false
	end

end
string_metatable.__index["stop_skill"] = skills.stop_skill



function skills.enable_skill(pl_name, skill_name)
	local skill = pl_name:get_skill(skill_name)

	if not skill then return false end

	return skill:enable()
end
string_metatable.__index["enable_skill"] = skills.enable_skill



function skills.disable_skill(pl_name, skill_name)
  local skill = pl_name:get_skill(skill_name)

	if not skill then return false end

	return skill:disable()
end
string_metatable.__index["disable_skill"] = skills.disable_skill



function skills.basic_checks_in_order_to_work(skill, ...)
	local active_blocking_skill = skills.blocking_skills[skill.pl_name]
	local is_blocked_by_another_skill = (
		active_blocking_skill
		and active_blocking_skill ~= skill.internal_name
		and skill.can_be_blocked_by_other_skills
	)

	local player = get_player_by_name(skill.pl_name)

	if not player then return false end
	if is_blocked_by_another_skill then return false end
	-- if a skill is looped can_cast only checks if it can start, it won't stop the cycle if
	-- the condition is false
	if skill["can_cast"] and not skill:can_cast(...) and not skill.is_active then
		return false
	end
	if skill.stop_on_death and player:get_hp() <= 0 then return false end

	if not skill.data._enabled then
		if skills.settings.chat_warnings.disabled ~= false then
			skills.error(skill.pl_name, S("You can't use the @1 skill now", skill.name))
		end

		return false
	end

	return true
end