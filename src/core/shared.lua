skills = {}

-- { "prefix1:skill1" = {...}, ...}
skills.registered_skills = {}

-- { "prefix1" = {...}, ...}
skills.prefix_configs = {}

--[[
    {
        "player": {
            {"prefix1:skill1" = {...}},
            {"prefix1:skill2" = {...}}
        }
    }
--]]
skills.player_skills = {}
