local function restore_celestial_vault(skill) end
local table_copy = table.copy


function skills.stop(self, cancelled)
	if not self.is_active or self.is_stopping then return false end
	if not core.get_player_by_name(self.pl_name) then return false end

	if self.blocks_other_skills and skills.blocking_skills[self.pl_name] == self.internal_name then
		skills.blocking_skills[self.pl_name] = nil
		skills.cast_passive_skills(self.pl_name)
	end

	-- I don't know. MT is weird or maybe my code is just bugged:
	-- without this after, if the skills ends very quickly the
	-- spawner and the sound simply... don't stop.
	local bgm = table_copy(self._bgm or {})
	local particles = table_copy(self._particles or {})
	local hud = table_copy(self._hud or {})

	self.is_stopping = true

	core.after(0, function()
		-- Stop sound
		skills.sound_stop(bgm)

		-- Remove particles
		if particles then
			for i, spawner_id in pairs(particles) do
				core.delete_particlespawner(spawner_id)
			end
		end

		-- Remove hud
		if hud then
			for name, id in pairs(hud) do
				self.player:hud_remove(id)
			end
		end

		self.is_stopping = false
		self.is_active = false
	end)

	self._bgm = {}
	self._hud = {}
	self._particles = {}
	self._stop_job = nil

	-- Reset physics
	if self.physics then
		local reverse = {
			["multiply"] = "divide",
			["divide"] = "multiply",
			["add"] = "sub",
			["sub"] = "add",
		}
		local operation = reverse[self.physics.operation] -- multiply/divide/add/sub

		for property, value in pairs(self.physics) do
			if property ~= "operation" then
				_G["skills"][operation .. "_physics"](self.pl_name, property, value)
			end
		end
	end

	-- Deapply monoids
	if self.monoids then
		skills.remove_monoids(self)
	end

	if not cancelled then
		skills.sound_play(self, self.sounds.stop, true)
		restore_celestial_vault(self)
		self:on_stop()
	end

	return true
end



function restore_celestial_vault(skill)
	local data = skill.data
	local cel_vault = skill.celestial_vault or {}

	-- Restore sky
	if cel_vault.sky and skill._sky then
		local pl = skill.player
		pl:set_sky(skill._sky)
		skill._sky = {}
	end

	-- Restore clouds
	if cel_vault.clouds and skill._clouds then
		local pl = skill.player
		pl:set_clouds(skill._clouds)
		skill._clouds = {}
	end

	-- Restore moon
	if cel_vault.moon and skill._moon then
		local pl = skill.player
		pl:set_moon(skill._moon)
		skill._moon = {}
	end

	-- Restore sun
	if cel_vault.sun and skill._sun then
		local pl = skill.player
		pl:set_sun(skill._sun)
		skill._sun = {}
	end

	-- Restore stars
	if cel_vault.stars and skill._stars then
		local pl = skill.player
		pl:set_stars(skill._stars)
		skill._stars = {}
	end
end
