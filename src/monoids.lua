function skills.apply_monoids(skill)
   if skill.monoids then
      skills.assert(core.get_modpath("player_monoids"), skill.internal_name .. ": player_monoids mod is not loaded but the skill is using it")

      for monoid_name, value in pairs(skill.monoids) do
         local monoid = value.ref
         local modificator = value.value
         local monoid_key = skill.internal_name .. "." .. monoid_name

         skills.assert(monoid, "Skill '" .. skill.internal_name .. "': ref for monoid '" .. monoid_name .. "' is nil, make sure the dependency is loaded and the name is spelled correctly")

         setmetatable(monoid, getmetatable(player_monoids.jump))
         
         skill._monoids = skill._monoids or {}
         monoid:add_change(skill.player, modificator, monoid_key)
         skill._monoids[monoid_key] = monoid
      end
   end
end



function skills.remove_monoids(skill)
   if skill._monoids then
      for monoid_key, ref in pairs(skill._monoids) do
         ref:del_change(skill.player, monoid_key)
         skill._monoids[monoid_key] = nil
      end
   end
end
