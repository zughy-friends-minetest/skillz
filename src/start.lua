local function change_celestial_vault(skill) end



function skills.start(self, ...)
	local attachments = self.attachments
	local loop_params = self.loop_params
	local sounds = self.sounds

	if not skills.basic_checks_in_order_to_work(self, ...) then
		self:stop()
		return false
	end

	-- not a loopable skill or already started
	if (not loop_params and not self.on_start and not self.on_stop) or self.is_active then
		return false
	end

	if self.cooldown_timer > 0 then
		skills.print_remaining_cooldown_seconds(self)
		return false
	end

	self.is_active = true

	-- Create hud
	if self.hud then
		self._hud = {}

		for i, hud_element in ipairs(self.hud) do
			local name = hud_element.name
			self._hud[name] = self.player:hud_add(hud_element)
		end
	end

	-- Attach entities
	if attachments.entities then
		for i, entity_def in ipairs(attachments.entities) do
			skills.attach_expiring_entity(self, entity_def)
		end
	end

	-- Change physics_override
	if self.physics then
		local operation = self.physics.operation -- multiply/divide/add/sub

		for property, value in pairs(self.physics) do
			if property ~= "operation" then
				_G["skills"][operation .. "_physics"](self.pl_name, property, value)
			end
		end
	end

	-- Apply monoids
	if self.monoids then
		skills.apply_monoids(self)
	end

	if self:on_start(...) == false then
		self:stop("cancelled")
		return false
	end

	-- Create particle spawners
	if attachments.particles then
		self._particles = {}

		for i, spawner in ipairs(attachments.particles) do
			spawner.attached = self.player
			self._particles[i] = core.add_particlespawner(spawner)
		end
	end

	change_celestial_vault(self)

	if self.blocks_other_skills then
		skills.blocking_skills[self.pl_name] = self.internal_name
		skills.block_other_skills(self)
	end

	-- Play sounds
	skills.sound_play(self, sounds.start, true)
	self._bgm = skills.sound_play(self, sounds.bgm)

	-- Stop skill after duration
	if loop_params and loop_params.duration then
		self._stop_job = core.after(loop_params.duration, function() self:stop() end)
	end

	skills.start_cooldown(self)

	if loop_params and loop_params.cast_rate then
		self:cast(...)
	end

	return true
end



function change_celestial_vault(skill)
	local cel_vault = skill.celestial_vault or {}

	-- Change sky
	if cel_vault.sky then
		local pl = skill.player
		skill._sky = pl:get_sky(true)
		pl:set_sky(cel_vault.sky)
	end

	-- Change moon
	if cel_vault.moon then
		local pl = skill.player
		skill._moon = pl:get_moon()
		pl:set_moon(cel_vault.moon)
	end

	-- Change sun
	if cel_vault.sun then
		local pl = skill.player
		skill._sun = pl:get_sun()
		pl:set_sun(cel_vault.sun)
	end

	-- Change stars
	if cel_vault.stars then
		local pl = skill.player
		skill._stars = pl:get_stars()
		pl:set_stars(cel_vault.stars)
	end

	-- Change clouds
	if cel_vault.clouds then
		local pl = skill.player
		skill._clouds = pl:get_clouds()
		pl:set_clouds(cel_vault.clouds)
	end
end
