dynamic_values = {
	-- key: skill_name.subtable1.subtable2.etc.key, values: dynamic value
}



skills.dynamic_properties_metatable = {
	-- filtering access to the table, getting dynamic values from an external table
	__index = function(t, key)
		local id = rawget(t, "_id")
		local skill = rawget(t, "_skill")

		if not id then
			skills.log("error", "skills.dynamic_properties_metatable.__index called without _id on table " .. dump(t), true)
		elseif not skill then
			skills.log("error", "skills.dynamic_properties_metatable.__index called without _skill on table " .. dump(t), true)
		end

		if dynamic_values[id] and dynamic_values[id][key] then
			return skills.get_value(skill, (dynamic_values[id] or {})[key])
		else
			return rawget(t, key)
		end
	end,

	-- never storing dynamic values, but storing them in a separate table instead
	__newindex = function(t, key, value)
		if skills.is_dynamic_value(value) then
			local id = rawget(t, "_id")
			dynamic_values[id] = dynamic_values[id] or {}
			dynamic_values[id][key] = value
			rawset(t, key, nil)
		else
			rawset(t, key, value)
		end
	end
}


function skills.does_table_contain_dynamic_values(t)
	for key, val in pairs(t) do
		if skills.is_dynamic_value(val) then return true end
		if type(val) == "table" then
			if skills.does_table_contain_dynamic_values(val) then return true end
		end
	end
	return false
end



function skills.make_dynamic_properties_table(skill, id, table, recursive, prev_table)
	if type(table) ~= "table" or not skills.does_table_contain_dynamic_values(table) then return end

	table._skill = skill
	local prev_id = (prev_table or {})._id or skill.internal_name
	table._id = prev_id .. "." .. id

	if recursive == nil then recursive = true end
	if recursive then
		for key, value in pairs(table) do
			-- ASSUMING: there are no other circular references other than _skill
			if type(value) == "table" and key ~= "_skill" then
				skills.make_dynamic_properties_table(skill, key, value, true, table)
			end
		end
	end

	-- reconstructing table, storing dynamic values in a separate table
	for key, value in pairs(table) do
		skills.dynamic_properties_metatable.__newindex(table, key, value)
	end

	return setmetatable(table, skills.dynamic_properties_metatable)
end



function skills.dynamic_value(func)
	return {
		dynamic_value = true,
		get_value = func
	}
end



function skills.get_value(skill, val)
	return val.get_value and val.get_value(skill) or val
end



function skills.is_dynamic_value(val)
	return type(val) == "table" and val.dynamic_value
end
