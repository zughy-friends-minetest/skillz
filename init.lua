skills = {}

dofile(core.get_modpath("skills") .. "/src/core/shared.lua")

dofile(core.get_modpath("skills") .. "/src/settings_loader.lua")

dofile(core.get_modpath("skills") .. "/src/utils.lua")

dofile(core.get_modpath("skills") .. "/src/database.lua")

dofile(core.get_modpath("skills") .. "/src/physics_manipulation.lua")
dofile(core.get_modpath("skills") .. "/src/monoids.lua")
dofile(core.get_modpath("skills") .. "/src/audio.lua")

dofile(core.get_modpath("skills") .. "/src/cooldown.lua")

dofile(core.get_modpath("skills") .. "/src/dynamic_properties.lua")

dofile(core.get_modpath("skills") .. "/src/cast.lua")
dofile(core.get_modpath("skills") .. "/src/start.lua")
dofile(core.get_modpath("skills") .. "/src/stop.lua")

dofile(core.get_modpath("skills") .. "/src/entities.lua")

dofile(core.get_modpath("skills") .. "/src/core/registration.lua")
dofile(core.get_modpath("skills") .. "/src/core/player.lua")
dofile(core.get_modpath("skills") .. "/src/core/instance.lua")

dofile(core.get_modpath("skills") .. "/src/commands.lua")

--dofile(core.get_modpath("skills") .. "/src/debug.lua")
