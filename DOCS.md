# Dev Documentation

**Note**: Functions that take `pl_name` as the first argument can also be called using the shorter method `pl_name:[function_name](...)` instead of `skills.[function_name](pl_name, ...)`.

## Table of Contents
1. [Registering a Prefix](#1-registering-a-prefix)
2. [Registering a Skill](#2-registering-a-skill)
    - [2.1 Dynamic properties](#21-dynamic-properties)
3. [Skills Based on Other Skills](#3-skills-based-on-other-skills)
4. [Skill Creation Guidelines](#4-skill-creation-guidelines)
    - [4.1 Skill Examples](#41-skill-examples)
5. [Assigning a Skill](#5-assigning-a-skill)
6. [Using a Skill](#6-using-a-skill)
7. [Utility Functions](#7-utility-functions)
   - [7.1. Skills Functions](#71-skills-functions)
   - [7.2. Player Functions](#72-player-functions)
   - [7.3. Entities Functions](#73-entities-functions)

---

## 1. Registering a Prefix

To register a prefix configuration, you can use the `skills.register_prefix_config(prefix, config)` function. The `prefix` argument is a unique prefix for your skills preceding your skill name (e.g. `myprefix:myskill`). The `config` argument is a definition table that can contain the following properties:
- **`base_layers`** (list of skill names): Optional. A list of base layers that will be applied to all skills registered with this prefix. Layers are special skills that are meant to be inherited by other skills and cannot be unlocked by players - although you can also use normal skills as layers.

Prefix configurations are optional but recommended to organize your skills. They must be declared before any skill or layer that uses them. Also, layers or base skills must be declared before skills using them.

Possible prefix patterns include:
- Simple mod organization: `mymod:skillname`
- Skill type organization: `mymod_passive:skillname`, `mymod_active:skillname`
- Class-based organization: `mymod_warrior:skillname`, `mymod_mage:skillname`
- ...etc

Each prefix can have its own base layers, allowing you to share common functionality between related skills. For example, all warrior skills could inherit combat-related layers, while mage skills inherit mana-related layers.

## 2. Registering a Skill

To register a skill, you can use the `skills.register_skill(internal_name, def)` function. The `internal_name` argument is the name you will use to refer to the skill in your code and should be formatted as `"unique_prefix:skill_name"` (where the unique prefix could be the name of your mod, for example). The `def` argument is a definition table that contains various properties that define the behavior and appearance of the skill and can contain the following properties:
- **`name`** (string): The skill's name.
- **`description`** (string): The skill's description.
- **`cooldown`** (number): The minimum amount of time (in seconds) to wait before casting the skill again.
- **`cast(self, ...)`**: Contains the skill's logic. It returns `false` if the player is offline or the cooldown has not finished. The `self` parameter is a table containing [properties of the skill](#6-using-a-skill).
- **`can_cast(self, ...)`**: Returns `true` by default. Must return a boolean indicating whether the skill can be cast or not.
- **`loop_params`** (table): If this is defined, the skill will be looped. To cast a looped skill, you need to use the `start(...)` function instead of `cast`. The `start` function simply calls the `cast` function at a rate of `cast_rate` seconds (if `cast_rate` is defined, otherwise the skill's logic will never be executed).
  - **`cast_rate`** (number): The rate at which the skill will be cast (in seconds). Assigning 0 will loop it as fast as possible.
  - **`duration`** (number): The amount of time (in seconds) after which the skill will stop. The `core.after` job that will stop it is stored in `self._stop_job`.
- **`stop_on_death`** (boolean): `true` by default. If set to `true`, the skill will automatically stop when the player dies.
- **`passive`** (boolean): `false` by default. If `true`, the skill will start automatically once the player has unlocked it.
  - It can be stopped by calling `stop()` or `disable()` and restarted by calling `start()` or `enable()`.
- **`blocks_other_skills`** (boolean): `false` by default. Whether this skill has to disable all other skills while in use.
- **`can_be_blocked_by_other_skills`** (boolean): `true` by default. Whether this skill will be disabled by other blocking skills (you may want to use this for a passive skill that just increases health, for example).
- **`monoids`** (table): Allows skills to modify global player state through [monoids](https://content.luanti.org/packages/Byakuren/player_monoids/). Define a table where each key is a unique name and value is a table containing:
    - **`ref`**: reference to the monoid object. It can be from `player_monoids` (e.g. `player_monoids.jump`), a third party monoid, or a custom one.
    - **`value`**: the modificator value to apply, structured however the said monoid wants it.

    Example: (using `player_monoids`)
    ```lua
    monoids = {
        jump = {
            ref = player_monoids.jump,
            value = 2  -- Double jump height
        },
        speed = {
            ref = player_monoids.speed,
            value = 1.5  -- 50% speed boost
        }
    }
    ```
    Example: (using the 3rd party monoid `lighting_monoid`)
    ```lua
    monoids = {
        dark_saturation = {
            ref  = lighting_monoid,
            value = {
                saturation = 2,
                exposure = {
                    luminance_min = 2
                }
            }
        }
    }
    ```
    Requires `player_monoids` mod to be installed and loaded. Changes are automatically applied on skill start and removed on skill stop. **Beware**, monoids are not meant to be used alongside mods that modify the player state manually: manual modifications will be overwritten.
- **`physics`** (table): This table can contain any [physics property](https://github.com/minetest/minetest/blob/master/doc/lua_api.md?plain=1#L8084)'s field and must contain an `operation` field having one of the following values: `add`, `sub`, `multiply`, `divide` (e.g., `{operation = "add", speed = 1}` will add 1 to the player's speed). Unlike the monoids approach, when the effect ends, the physics properties will simply be mathematically reverted, meaning that if the player's original physics values were not the default ones, they will be restored correctly, without overriding previous manual modifications. This may be a better approach when interacting with mods that don't use monoids.
- **`sounds`** (table): A sound or a list of sounds that will be played when a certain event triggers them. Every sound is a table and is declared this way:
  - **`name`**: The sound's name.
  - **Optional Parameters**:
    - **`to_player`**: `false` by default. If set to `true`, the value will become the player's name.
    - **`object`**: `true` by default. If set to `true`, the value will become the player's `ObjectRef`.
    - **`exclude_player`**: `false` by default. If set to `true`, the value will exclude the player's `ObjectRef`.
    - **`random_pitch`**: `{min, max}`. A custom property that will choose a random pitch between `min` and `max` every time the sound is played.
    - **Other Sound Parameters/SoundSpec Properties**: You can include additional sound parameters as needed.
  - You can also pass a list of sounds; the mod will play them all.
  - The sound(s) or sounds list must be placed inside the desired event subtable, which can be:
    - **`cast`**: Played every time the `cast` function is called.
    - **`start`**: Played when the skill starts.
    - **`stop`**: Played when the skill stops.
    - **`bgm`**: Looping sound(s), played while the skill is being used.
- **`hud`** (`{{name = "hud_name", standard hud definition params...}, ...}`): A list of HUD elements that appear while the skill is being used. They are stored in the `self._hud` table (`{"hud_name" = hud_id}`).
- **`attachments`** (table):
  - **`particles`** (`{ParticleSpawner1, ...}`): A list of particle spawners that are created when the skill starts and destroyed when it stops. They're stored in the `data._particles` table and are always attached to the player—useful if you want to have something like a particle trail.
  - **`entities`** (`{Entity1, ...}`): A list of entities that are attached to the player as long as the skill is being used. The entities are tables, declared this way:
    - **`pos`**: The player-relative position.
    - **`name`**: The name of the entity.
    - **Optional Parameters**:
      - **`bone`**: The bone to which the entity is attached.
      - **`rotation`**: Rotation of the entity.
      - **`forced_visible`**: Whether the entity is forcibly visible or not.
    - Their behavior is equivalent to [registering the entity as expirable, adding it as soon as the skill starts, and attaching it to the player](#73-entities-functions).
- **`celestial_vault`** (table): Changes the player's view of the sky while the skill is active. Can contain:
  - **`sky`**: Changes the sky appearance
  - **`moon`**: Changes the moon appearance
  - **`sun`**: Changes the sun appearance
  - **`stars`**: Changes the stars appearance
  - **`clouds`**: Changes the clouds appearance
  Each property takes the same parameters as their corresponding Minetest set_* functions (set_sky, set_moon, etc). The original values are automatically restored when the skill stops.


### 2.1 Dynamic Properties
When defining a skill, you can make certain properties have dynamic values that update based on the current state. Dynamic values can be used in the following tables of your skill definition:

- `loop_params`
- `sounds`
- `hud`
- `attachments`
- `physics`
- `celestial_vault`

To create a dynamic value, use `skills.dynamic_value(function(skill) end)`. The function receives the current skill as an argument and should return the desired value.

Example:
```lua
skills.register_skill("example:show_hp", {
    name = "Show HP",
    loop_params = {
        cast_rate = 0.25
    },
    hud = {{
        name = "hp",
        hud_elem_type = "text",
        position = {x = 0.5, y = 0.5},
        scale = {x = 100, y = 100},
        text = skills.dynamic_value(function(skill)
            return "HP: " .. skill.player:get_hp()
        end),
    }},
    redraw_hud = function(self)
        --                                                        vv
        self.player:hud_change(self._hud.hp, "text", self.hud[1].text)
    end,
    cast = function(self)
        self:redraw_hud()
    end,
})
```

Notice how you can access the dynamic value by simply accessing the corresponding table field.


## 3. Skills Based on Other Skills
[🌐 Visual representation.](https://i.imgur.com/BaZxFsG.jpeg)

A skill based on another skill is a modified version that retains some of the original skill's properties, while keeping others the same. The original skills can be normal ones or layers. A layer is nothing more than a special type of skill that players can't unlock, and that won't be returned by `get_registered_skills()`. You can register one by using:
- **`skills.register_layer(internal_name, def)`**: it works exactly like register_skill.  

And then register the skill using:
- **`skills.register_skill_based_on({"mod:base_skill_1", "mod:base_skill_2", ...}, "mod:new_skill_name", def)`**.

The def table of the second function allows you to override any properties of the original skill(s) that you want to change in the new skill (any non-specified properties will be inherited from the original(s)). If you want to override one of the properties with a `nil` value just set it to `"@@nil"`.

Also, when specifying more then one base skill, their functions will be joined together and the first parameter will always be `self`. The same will happen if you define a new function in the def table (e.g. if you're creating a "s3" skill based on "s1" and "s2", and you defined `cast()` in each one of them, the joined cast function will have the following structure: `s1_cast(self, ...); s2_cast(same); s3_cast(same)`). If any of those functions return false, the following ones won't be called (this applies for every function in the skill: they will all be merged, even custom ones!).

Beware, when you combine skills it's up to you to make sure they can work together, so make sure they have the same cast_rate, don't overwrite each other's parameters, etc.

This type of skill can be useful to modularize behavior: a layer can manage checks common to certain types of skills, such as if the players has enough mana, if they can cast an ultimate, etc. It can also be useful to create reusable and extendible skill templates.

## 4. Skill Creation Guidelines

Here are guidelines to help you design your skill:

- **Undo Temporary Changes**: Undo any temporary changes applied in `on_start` within `on_stop`.
- **Initialize Skill Data**: Reset or initialize skill data in `on_start`, since `on_stop` won't be called if the server crashes while a skill is being cast.
- **Avoid Abusing Skill Data**: Skill data gets serialized into mod storage; use it judiciously.
- **Modularize Skill Behavior**:
  - If you have ultimate skills that require 5 kills, move the kills check to a separate layer.
  - If you have multiple ways of starting a skill, manage input in a separate layer.
  - etc...
- **Declare Custom Functions**: You can declare your own functions in the skill definition to keep the main callbacks clean and readable.
- **Enhance Graphics and Audio**: Don't neglect visuals and sounds—use `attachments`, `hud`, and `celestial_vault` to enhance the skill experience!

### 4.1 Skill Examples
Here are some examples of skills
<details>
<summary>click to expand...</summary>

#### Example 1: Simple Counter Skill

```lua
skills.register_skill("example_mod:counter", {
    name = "Counter",
    description = "Counts. You can use it every 2 seconds.",
    sounds = {
        cast = {name = "ding", pitch = 2}
    },
    cooldown = 2,
    data = {
        counter = 0
    },
    cast = function(self)
        self.data.counter = self.data.counter + 1
        print(self.pl_name .. " is counting: " .. self.data.counter)
    end
})
```

#### Example 2: Heal Over Time

```lua
skills.register_skill("example_mod:heal_over_time", {
    name = "Heal Over Time",
    description = "Restores a heart every 3 seconds for 30 seconds.",
    loop_params = {
        cast_rate = 3,
        duration = 30
    },
    sounds = {
        cast = {name = "heart_added"},
        bgm = {name = "angelic_music"}
    },
    cast = function(self)
        local player = self.player
        player:set_hp(player:get_hp() + 2)
    end
})
```

#### Example 3: Boost Physics

```lua
skills.register_skill("example_mod:boost_physics", {
    name = "Boost Physics",
    description = "Multiplies the speed and gravity by 1.5 for 3 seconds.",
    loop_params = {
        duration = 3
    },
    sounds = {
        start = {name = "speed_up"},
        stop = {name = "speed_down"}
    },
    physics = {
        operation = "multiply",
        speed = 1.5,
        gravity = 1.5
    }
})
```

#### Example 4: Set Speed (Passive Skill)

```lua
skills.register_skill("example_mod:set_speed", {
    name = "Set Speed",
    description = "Sets speed to 3.",
    passive = true,
    data = {
        original_speed = {}
    },
    on_start = function(self)
        local player = self.player
        self.data.original_speed = player:get_physics_override().speed

        player:set_physics_override({speed = 3})
    end,
    on_stop = function(self)
        self.player:set_physics_override({speed = self.data.original_speed})
    end
})
```

#### Example 5: Iron Skin

```lua
skills.register_skill("example_mod:iron_skin", {
    name = "Iron Skin",
    description = "Take half the damage for 6 seconds.",
    cooldown = 20,
    loop_params = {
        duration = 6,
    },
    sounds = {
        start = {name = "iron_skin_on", max_hear_distance = 6},
        stop = {name = "iron_skin_off", max_hear_distance = 6},
    },
    attachments = {
        entities = {{
            name = "example_mod:iron_skin",
            pos = {x = 0, y = 22, z = 0}
        }}
    },
    hud = {{
        name = "shield",
        hud_elem_type = "image",
        text = "hud_iron_skin.png",
        scale = {x = 3, y = 3},
        position = {x = 0.5, y = 0.82},
    }},
})
```
</details>


## 5. Assigning a Skill

To unlock or remove a skill from a player, use the `skills.unlock_skill(pl_name, skill_name)` or `skills.remove_skill(pl_name, skill_name)` functions.


## 6. Using a Skill

To use a player's skill, you can choose between the short method or the long method.

### Short Method

Use the following functions:

- `pl_name:cast_skill("skill_name", [...])`
- `pl_name:start_skill("skill_name", [...])`
- `pl_name:stop_skill("skill_name")`

If the player can't use the skill (e.g., they haven't unlocked it), these functions will return `false`.

### Long Method

First, get the player's skill table using `skills.get_skill(pl_name, "skill_name")`. If the player can't use the skill, this will return `false`.

The function will return the player's skill table, composed of the [definition properties](#2-registering-a-skill) plus the following new properties:

- **`disable()`**: Disables the skill; when disabled, the `cast` and `start` functions won't work.
- **`enable()`**: Enables the skill.
- **`data._enabled`** (boolean): `true` if the skill is enabled.
- **`internal_name`** (string): The name used to refer to the skill in the code.
- **`cooldown_timer`** (number): The time left until the end of the cooldown.
- **`is_active`** (boolean): `true` if the skill is active.
- **`pl_name`** (string): The name of the player using this skill.
- **`player`** (ObjectRef): The player using this skill. **Only use this while the skill is being cast!**

Once you have the skill table, you can call:

- `skill_table:cast([...])` or
- `skill_table:start([...])`

to cast the skill. To stop it, use:

- `skill_table:stop()`


## 7. Utility Functions

### 7.1. Skills Functions

- **`skills.register_on_unlock(function(skill_table), [prefix])`**: Called every time a player unlocks a skill with the specified prefix. If the prefix isn't specified, the function will be called every time a player unlocks a skill.
- **`skills.disable_skill(pl_name, skill_name)`**: Short method to disable a skill.
- **`skills.enable_skill(pl_name, skill_name)`**: Short method to enable a skill.
- **`skills.get_skill_def(skill_name)`**: Returns the skill's definition table.
- **`skills.does_skill_exist(skill_name)`**: Checks if a skill exists (not case-sensitive). Always returns `false` for layers.
- **`skills.get_registered_skills([prefix])`**: Returns the registered non-layer skills. If a prefix is specified, only the skills with that prefix will be listed (`{"prefix1:skill1" = {def}}`).
- **`skills.get_registered_layers([prefix])`**: Same as above but returns a list of layers.
- **`skills.get_unlocked_skills(pl_name, [prefix])`**: Returns the unlocked skills for a player. If a prefix is specified, only the skills with that prefix will be listed (`{"prefix1:skill1" = {def}}`).
- **`skills.has_skill(pl_name, skill_name)`**: Returns `true` if the player has the skill.
- **`skills.for_each_player_in_db(callback)`**: Calls `callback(pl_name, skills)` for each player in the database. Always use this function if you need to operate on many offline players to avoid performance issues.

### 7.2. Player Functions

- **`skills.add_physics(pl_name, property, value)`**
- **`skills.sub_physics(pl_name, property, value)`**
- **`skills.multiply_physics(pl_name, property, value)`**
- **`skills.divide_physics(pl_name, property, value)`**

These functions modify the specified [physics property](https://github.com/minetest/minetest/blob/master/doc/lua_api.md?plain=1#L8084) of the player by adding, subtracting, multiplying, or dividing the given `value`.

**Example:**

```lua
pl_name:add_physics("speed", 1)
```

### 7.3. Entities Functions

- **`skills.register_expiring_entity(name, def)`**: Registers an entity that's meant to exist only while a skill is active.

  Such an entity will have its `static_save` parameter set to `false`, and its Lua table will contain three additional parameters:

  - **`pl_name`** (string)
  - **`player`** (ObjectRef)
  - **`skill`** (table): A reference to the skill that spawned the entity.

  The `staticdata` passed to the entity's `on_activate()` is a serialized `{pl_name = "...", skill_name = "..."}`. When the linked skill stops, this entity will be removed, calling the entity's custom callback `before_remove(self)` (to be defined in the entity's definition table).

- **`skills.add_entity(skill, pos, name)`** / **`skill:add_entity(pos, name)`**: Spawns an expiring entity, linking it to the selected skill. It returns the entity's `ObjectRef`.
